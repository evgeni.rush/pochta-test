﻿using System.Security.Cryptography;
using System.Text;

namespace Test.Database.Utils
{
    public class Utilities
    {
        /// <summary>
        /// Computes hash of given string with given algorithm
        /// </summary>
        /// <param name="alg"> algorithm </param>
        /// <param name="input"> input string </param>
        /// <returns></returns>
        public static string ComputeHash(HashAlgorithm alg, string input)
        {
            var data = alg.ComputeHash(Encoding.UTF8.GetBytes(input));
            var sBuilder = new StringBuilder();
            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}