﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Test.Database.Options;

namespace Test.Database.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddTestDatabase(this IServiceCollection services,
            Action<TestDbOptions> configure)
        {
            var options = new TestDbOptions();

            configure?.Invoke(options);

            services.AddDbContext<TestDbContext>(
                opt => { opt.UseNpgsql(options.ConnectionString); },
                contextLifetime: ServiceLifetime.Transient,
                optionsLifetime: ServiceLifetime.Singleton);

            return services;
        }
    }
}