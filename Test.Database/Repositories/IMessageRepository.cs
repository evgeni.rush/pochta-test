﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Shared.Enums;
using Shared.Models;

namespace Test.Database.Repositories
{
    public interface IMessageRepository
    {
        Task<Message> PushIncomingMessageAsync(Message msg);

        Task<Message> PushOutgoingMessageAsync(Message msg);

        Task<string> GetOutgoingMessagesHashAsync();

        Task<string> GetIncomingMessagesHashAsync();

        Task<Message> PeekIntoFirstOutgoingMessageAsync();

        Task<Message> PopOutgoingMessageAsync();

        Task<IEnumerable<Message>> GetMessagesByStatus(MessageStatus status, int count, int offset);
    }
}