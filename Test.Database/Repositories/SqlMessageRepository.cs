﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shared.Enums;
using Shared.Models;
using Test.Database.Utils;

namespace Test.Database.Repositories
{
    public class SqlMessageRepository : IMessageRepository
    {
        public TestDbContext Context { get; set; }

        public SqlMessageRepository(TestDbContext context)
        {
            Context = context;
        }

        public async Task<Message> PushIncomingMessageAsync(Message msg)
        {
            var entityEntry = Context.Messages.Update(msg);
            await Context.SaveChangesAsync();
            return entityEntry.Entity;
        }

        public async Task<Message> PushOutgoingMessageAsync(Message msg)
        {
            msg.DbHash = await GetOutgoingMessagesHashAsync();
            var entityEntry = await Context.Messages.AddAsync(msg);
            await Context.SaveChangesAsync();
            return entityEntry.Entity;
        }

        public async Task<string> GetOutgoingMessagesHashAsync()
        {
            var list = await Context.Messages.AsNoTracking().Where(m => m.Status == MessageStatus.Created)
                .ToListAsync();
            var md5Hash = MD5.Create();

            var input = list.Aggregate(string.Empty, (current, element) => string.Concat(current, element.ToString()));

            return Utilities.ComputeHash(md5Hash, input);
        }

        public async Task<string> GetIncomingMessagesHashAsync()
        {
            var list = await Context.Messages.AsNoTracking().Where(m => m.Status == MessageStatus.Received)
                .ToListAsync();
            var md5Hash = MD5.Create();

            var input = list.Aggregate(string.Empty, (current, element) => string.Concat(current, element.ToString()));

            return Utilities.ComputeHash(md5Hash, input);
        }

        public async Task<Message> PeekIntoFirstOutgoingMessageAsync()
        {
            var msg = await Context.Messages.AsNoTracking().Where(m => m.Status == MessageStatus.Created)
                .OrderByDescending(e => e.Id).FirstOrDefaultAsync();

            return msg;
        }

        public async Task<Message> PopOutgoingMessageAsync()
        {
            var msg = await Context.Messages.AsNoTracking().Where(m => m.Status == MessageStatus.Created)
                .OrderByDescending(e => e.Id).FirstOrDefaultAsync();

            if (msg != default)
            {
                Context.Messages.Remove(msg);
                await Context.SaveChangesAsync();
            }

            return msg;
        }

        public async Task<IEnumerable<Message>> GetMessagesByStatus(MessageStatus status, int count, int offset)
        {
            var messages = await Context.Messages.AsNoTracking()
                .Where(m => m.Status == status)
                .Skip(offset)
                .Take(count)
                .ToListAsync();

            return messages;
        }
    }
}