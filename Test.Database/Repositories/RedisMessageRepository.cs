﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Shared.Enums;
using Shared.Models;
using StackExchange.Redis;
using Test.Database.Utils;

namespace Test.Database.Repositories
{
    public class RedisMessageRepository : IMessageRepository
    {
        private readonly IDatabase _db;
        private const string OutgoingMessagesQueueName = "OutgoingMessages";
        private const string OutgoingMessagesIdName = "OutgoingMessagesId";
        private const string IncomingMessagesQueueName = "IncomingMessages";

        /// <summary>
        /// Constructor. Connects to local redis server.
        /// </summary>
        public RedisMessageRepository()
        {
            var redis = ConnectionMultiplexer.Connect("localhost");
            _db = redis.GetDatabase();
        }

        /// <summary>
        /// Puts incoming message to the right of queue.
        /// </summary>
        /// <param name="msg"> Incoming message </param>
        public async Task<Message> PushIncomingMessageAsync(Message msg)
        {
            await _db.ListRightPushAsync(new RedisKey(IncomingMessagesQueueName),
                new RedisValue(JsonConvert.SerializeObject(msg)));
            return msg;
        }

        /// <summary>
        /// Increments message counter and pushes next message to the right of queue.
        /// </summary>
        /// <param name="msg"></param>
        public async Task<Message> PushOutgoingMessageAsync(Message msg)
        {
            _db.StringIncrement(new RedisKey(OutgoingMessagesIdName));
            msg.Id = int.Parse(_db.StringGet(new RedisKey(OutgoingMessagesIdName)).ToString());
            msg.DbHash = await GetOutgoingMessagesHashAsync();
            _db.ListRightPush(new RedisKey(OutgoingMessagesQueueName), new RedisValue(JsonConvert.SerializeObject(msg)));

            return msg;
        }

        /// <summary>
        /// Computes hash of all outgoing messages
        /// </summary>
        /// <returns> md5 hash </returns>
        public async Task<string> GetOutgoingMessagesHashAsync()
        {
            var list = await _db.ListRangeAsync(OutgoingMessagesQueueName);
            var md5Hash = MD5.Create();

            var input = list.Aggregate(string.Empty, (current, element) => string.Concat(current, element.ToString()));

            return Utilities.ComputeHash(md5Hash, input);
        }

        /// <summary>
        /// Computes hash of all incoming messages
        /// </summary>
        /// <returns> md5 hash </returns>
        public async Task<string> GetIncomingMessagesHashAsync()
        {
            var list = await _db.ListRangeAsync(IncomingMessagesQueueName);
            var md5Hash = MD5.Create();

            var input = list.Aggregate(string.Empty, (current, element) => string.Concat(current, element.ToString()));

            return Utilities.ComputeHash(md5Hash, input);
        }

       

        /// <summary>
        /// Returns left-most element of queue without removing it
        /// </summary>
        /// <returns> Left-most element </returns>
        public async Task<Message> PeekIntoFirstOutgoingMessageAsync()
        {
            var val = await _db.ListGetByIndexAsync(new RedisKey(OutgoingMessagesQueueName), 0); //first value

            Message msg = null;
            try
            {
                if (!val.IsNullOrEmpty)
                    msg = JsonConvert.DeserializeObject<Message>(val.ToString());
            }
            catch
            {
                /*ignore*/
            }

            return msg;
        }

        /// <summary>
        /// Removes left-most element from queue and returns it to caller
        /// </summary>
        /// <returns> Left-most message </returns>
        public async Task<Message> PopOutgoingMessageAsync()
        {
            var val = await _db.ListLeftPopAsync(new RedisKey(OutgoingMessagesQueueName)); //first value

            Message msg = null;
            try
            {
                msg = JsonConvert.DeserializeObject<Message>(val.ToString());
            }
            catch
            {
                /*ignore*/
            }

            return msg;
        }

        public Task<IEnumerable<Message>> GetMessagesByStatus(MessageStatus status, int count, int offset)
        {
            throw new System.NotImplementedException();
        }
    }
}