﻿using Microsoft.EntityFrameworkCore;
using Shared;
using Shared.Models;

namespace Test.Database
{
    public class TestDbContext : DbContext
    {
        public TestDbContext(DbContextOptions<TestDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Message> Messages { get; set; }
        
    }
}