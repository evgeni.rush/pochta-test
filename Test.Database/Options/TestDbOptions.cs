﻿namespace Test.Database.Options
{
    public class TestDbOptions
    {
        public string ConnectionString { get; set; }
    }
}