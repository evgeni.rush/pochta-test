FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y redis-server apt-transport-https wget software-properties-common git && \
    apt-get clean

RUN wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb

RUN apt-get update
RUN apt-get install -y dotnet-sdk-3.1 

RUN mkdir src

WORKDIR src

ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache
RUN git clone https://gitlab.com/evgeni.rush/pochta-test.git

RUN chmod +x pochta-test/docker/start-producer.sh
CMD ./pochta-test/docker/start-producer.sh


