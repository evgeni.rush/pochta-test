﻿namespace Shared.Enums
{
    public enum MessageStatus
    {
        Created,
        Received
    }
}