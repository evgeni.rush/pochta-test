﻿using System;
using System.Collections.Concurrent;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NATS.Client;
using Newtonsoft.Json;
using Shared.Models;

namespace Shared
{
    public class BasePublisher : IDisposable
    {
        protected object SyncLock = new object();

        protected BlockingCollection<string> respQueue = new BlockingCollection<string>();
        protected IConnection Connection { get; set; }

        public string SubjectName { get; set; }

        public BasePublisher()
        {
            SubjectName = Guid.NewGuid().ToString();
        }

        protected void InitializeConnection(string url)
        {
            string[] servers =
            {
                url
            };

            Options opts = ConnectionFactory.GetDefaultOptions();
            opts.MaxReconnect = 2;
            opts.ReconnectWait = 1000;
            opts.NoRandomize = true;
            opts.Servers = servers;

            Connection = new ConnectionFactory().CreateConnection(opts);

            EventHandler<MsgHandlerEventArgs> h = (sender, args) =>
            {
                Console.WriteLine(args.Message);


                var response = Encoding.UTF8.GetString(args.Message.Data);

                respQueue.Add(response);
            };

            IAsyncSubscription s = Connection.SubscribeAsync(SubjectName, h);
        }

        protected string WaitForBaseResponse(int timeOut)
        {
            var result = respQueue.TryTake(out var responseBytes, timeOut);
            if (!result)
                throw new Exception("Timeout");

            var br = JsonConvert.DeserializeObject<BaseResponse>(responseBytes);
            return br.Status;
        }

        public void Exit(string subjectName, int timeOut,
            out string message)
        {
            var c = new BaseRequest("Exit");
            var json = JsonConvert.SerializeObject(c);

            var messageBytes = Encoding.UTF8.GetBytes(json);

            Connection.Publish(subjectName, messageBytes);

            message = WaitForBaseResponse(timeOut);
        }

        public string Exchange(string json, string sendTo, double timeOutMs = 200)
        {
            lock (SyncLock)
            {
                var timeout = TimeSpan.FromMilliseconds(timeOutMs);

                while (respQueue.TryTake(out var _))
                {
                } // clean resp queue

                var messageBytes = Encoding.UTF8.GetBytes(json);

                Connection.Publish(sendTo, SubjectName, messageBytes);

                var result = respQueue.TryTake(out string responseBytes, timeout);

                if (!result)
                {
                    throw new Exception("Response not received in specified time.");
                }


                return responseBytes;
            }
        }

        public void Dispose()
        {
            Connection?.Drain();
            Connection?.Close();
        }
    }
}