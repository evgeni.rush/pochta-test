﻿using System;
using System.Collections.Generic;
using System.Text;
using NATS.Client;
using Newtonsoft.Json;
using Shared.Models;

namespace Shared
{
    public class BaseHandler
    {
        protected Dictionary<string, Func<string, BaseResponse>> RequestHandlers;

        public event EventHandler ExitReceived;
        protected IConnection Connection { get; set; }

        public string SubjectName { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="subjectName"> Subject to subscribe </param>
        public BaseHandler(string subjectName)
        {
            SubjectName = subjectName ?? "Consumer";


            RequestHandlers = new Dictionary<string, Func<string, BaseResponse>>
            {
                {"Exit", HandleExit}
            };
        }

        /// <summary>
        /// Initializes connection with given url
        /// </summary>
        /// <param name="url"> Server's url </param>
        public void InitializeConnection(string url)
        {
            string[] servers =
            {
                url
            };

            Options opts = ConnectionFactory.GetDefaultOptions();
            opts.MaxReconnect = 2;
            opts.ReconnectWait = 1000;
            opts.NoRandomize = true;
            opts.Servers = servers;

            Connection = new ConnectionFactory().CreateConnection(opts);
        }

        /// <summary>
        /// Handles "Exit" Message. Invokes exit event, that could be used to exit service.
        /// </summary>
        /// <param name="message"> serialized request </param>
        /// <returns></returns>
        protected BaseResponse HandleExit(string message)
        {
            var handler = ExitReceived;
            handler?.Invoke(this, EventArgs.Empty);

            return new BaseResponse("Exit Response");
        }

        /// <summary>
        ///  Creates event handler with given functions, subscribes it to current subject.
        /// </summary>
        /// <param name="requestHandlers"> Handlers </param>
        protected void RegisterRequestHandlers(Dictionary<string, Func<string, BaseResponse>> requestHandlers)
        {
            EventHandler<MsgHandlerEventArgs> h = (sender, args) =>
            {
                var body = args.Message.Data;
                var message = Encoding.UTF8.GetString(body);

                var replyTo = args.Message.Reply;

                var bc = JsonConvert.DeserializeObject<BaseRequest>(message);

                var status = "Successful";

                var br = new BaseResponse("", status);
                try
                {
                    if (requestHandlers.ContainsKey(bc.Name))
                    {
                        br = requestHandlers[bc.Name](message);
                    }
                    else
                    {
                        throw new InvalidOperationException("Unknown Command.");
                    }
                }
                catch (Exception ex)
                {
                    status = ex.Message;
                }
                finally
                {
                    br.Status = status;
                    var json = JsonConvert.SerializeObject(br);

                    var responseBytes = Encoding.UTF8.GetBytes(json);

                    Connection.Publish(replyTo, responseBytes);
                }
            };

            Connection.SubscribeAsync(SubjectName, h);
        }
    }
}