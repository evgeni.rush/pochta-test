﻿using System;
using Shared.Enums;

namespace Shared.Models
{
    public class Message
    {
        public int Id { get; set; }
        public DateTime SendTime { get; set; }
        public DateTime ReceiveTime { get; set; }
        public string Text { get; set; }
        public string DbHash { get; set; }
        public MessageStatus Status { get; set; }
    }
}