﻿namespace Shared.Models
{
    public class BaseResponse : BaseRequest
    {
        public BaseResponse(string name, string status = "Successful") : base(name)
        {
            Status = status;
        }

        public string Status { get; set; }
    }
}