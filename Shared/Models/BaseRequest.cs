﻿namespace Shared.Models
{
    public class BaseRequest
    {
        public BaseRequest(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}