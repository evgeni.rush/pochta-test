﻿namespace Shared.Models
{
    public class SendMessageRequest : BaseRequest
    {
        public Message Message { get; set; }

        public SendMessageRequest(Message message) : base("Send Message")
        {
            Message = message;
        }
    }

}