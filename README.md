# pochta-test

Для запуска проекта понадобится docker
https://hub.docker.com/editions/community/docker-ce-desktop-windows/

из папки pochta-test/docker выполняем

1) Создаем виртуальную сеть для nats

`docker network create nats`

2) Собираем и запускаем nats сервер

`docker-compose -f .\nats-cluster.yaml up -d`

3) Собираем и запускаем докер для потребителя

`docker build -f consumer.dockerfile -t consumer-ubuntu  .`


`docker run -it -rm --network nats --name consumer consumer-ubuntu`


3) Собираем и запускаем докер для производителя

`docker build -f producer.dockerfile -t producer-ubuntu  .`

`docker run -it --network nats --name producer producer-ubuntu`