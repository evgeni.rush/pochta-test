﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shared.Enums;

namespace Test.Consumer.API.Forms
{
    public class GetMessagesByStatusForm
    {
        public MessageStatus Status { get; set; }
        public int Count { get; set; }
        public int Offset { get; set; }
    }
}
