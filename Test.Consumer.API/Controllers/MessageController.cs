﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shared.Models;
using Test.Consumer.API.Forms;
using Test.Database.Repositories;

namespace Test.Consumer.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessageController : ControllerBase
    {

        private readonly ILogger<MessageController> _logger;
        private readonly IMessageRepository _messageRepository;

        public MessageController(ILogger<MessageController> logger, IMessageRepository messageRepository)
        {
            _logger = logger;
            _messageRepository = messageRepository;
        }

        [HttpGet("list")]
        public async Task<IEnumerable<Message>> GetMessagesByStatus([FromQuery] GetMessagesByStatusForm form)
        {
            var messages = await _messageRepository.GetMessagesByStatus(form.Status, form.Count, form.Offset);
            return messages;
        }
    }
}
