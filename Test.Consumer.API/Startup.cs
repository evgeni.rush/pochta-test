using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Shared;
using Test.Consumer.API.Configuration;
using Test.Consumer.API.Services;
using Test.Database.Extensions;
using Test.Database.Options;
using Test.Database.Repositories;

namespace Test.Consumer.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment hostEnvironment)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<SubscriptionOptions>(Configuration.GetSection(nameof(SubscriptionOptions)));

            var connectionString = Configuration.GetConnectionString("TestDbConnectionString");

            
            services.AddTransient<IMessageRepository, SqlMessageRepository>();

            services.AddTransient<IHostedService, Handler>();
            services.AddTransient<IMessageRepository, SqlMessageRepository>();

            services.AddTestDatabase(opt => opt.ConnectionString = connectionString);

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseFileServer(new FileServerOptions()
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "StaticFiles")),
                RequestPath = "/StaticFiles"
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}