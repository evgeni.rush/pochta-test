﻿namespace Test.Consumer.API.Configuration
{
    public class SubscriptionOptions
    {
        public string SubjectToSubscribe { get; set; }

        public string Url { get; set; }
    }
}