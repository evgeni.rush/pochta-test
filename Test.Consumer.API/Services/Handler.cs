﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Shared;
using Shared.Enums;
using Shared.Models;
using Test.Consumer.API.Configuration;
using Test.Database.Repositories;

namespace Test.Consumer.API.Services
{
    public class Handler : BaseHandler, IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private readonly IOptions<SubscriptionOptions> _config;

        private IMessageRepository MessageRepository { get; }
        public CancellationTokenSource CancelTokenSource { get; set; }

        public Handler(ILogger<Handler> logger, IOptions<SubscriptionOptions> config, IMessageRepository messageRepository)
            : base(config?.Value.SubjectToSubscribe)
        {
            _logger = logger;
            _config = config;
            MessageRepository = messageRepository;

            RequestHandlers.Add("Send Message", HandleSendMessageRequest);
        }

        /// <summary>
        /// Continuously checks connection and reconnects if it is not available
        /// </summary>
        /// <param name="url"> Server's url </param>
        /// <returns> Task object </returns>
        private async Task MaintainConnectionAsync(string url, CancellationToken ctx)
        {
            while (!ctx.IsCancellationRequested)
            {
                while (Connection == null || Connection.IsClosed())
                {
                    _logger?.LogInformation($"Trying to connect to {url}");
                    try
                    {
                        InitializeConnection(url);
                        RegisterRequestHandlers(RequestHandlers);

                        _logger.LogInformation($"Successfully connected to {url}");
                    }
                    catch (Exception e)
                    {
                        _logger?.LogError($"Exception occurred while trying to connect {e.Message}");
                    }


                    await Task.Delay(1000, ctx);
                }
            }
        }


        /// <summary>
        /// Handles any request with "Send Message" name
        /// </summary>
        /// <param name="str"> Serialized request </param>
        /// <returns> BaseResponse object </returns>
        private BaseResponse HandleSendMessageRequest(string str)
        {
            var req = JsonConvert.DeserializeObject<SendMessageRequest>(str);
            var msg = req.Message;

            _logger?.LogInformation(
                $"Id = {msg.Id} Text = {msg.Text} SendTime {msg.SendTime} DbHash {msg.DbHash}");

            msg.ReceiveTime = DateTime.UtcNow;
            msg.Status = MessageStatus.Received;
         

            MessageRepository?.PushIncomingMessageAsync(msg).Wait();

            return new BaseResponse("Send Message Response");
        }


        public void Dispose()
        {
            _logger?.LogInformation("Disposing....");
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger?.LogInformation("Starting service: " + _config.Value.SubjectToSubscribe);

            var url = _config?.Value.Url ?? "localhost:4222"; // for debug purposes

            CancelTokenSource = new CancellationTokenSource();
            var token = CancelTokenSource.Token;

            Task.Run(() => MaintainConnectionAsync(url, token), token);
            _logger?.LogInformation($"Successfully connected to {url}");

            return Task.CompletedTask;
        }


        public Task StopAsync(CancellationToken cancellationToken)
        {
            CancelTokenSource?.Cancel();

            _logger?.LogInformation("Stopping service.");
            return Task.CompletedTask;
        }
    }
}