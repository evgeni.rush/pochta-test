﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Shared;
using Shared.Enums;
using Shared.Models;
using Test.Database.Repositories;

namespace Test.Producer
{
    public class Generator : IHostedService
    {
        private readonly IServiceProvider _provider;
        private readonly ILogger _logger;

        private Thread _thread;
        private bool _stop;
        private IMessageRepository MessageRepository { get; set; }

        /// <summary>
        /// Constructor. Creates generator object.
        /// </summary>
        /// <param name="provider"> Service provider </param>
        /// <param name="logger"> Logger </param>
        public Generator(IMessageRepository messageRepository, ILogger<Generator> logger)
        {
            MessageRepository = messageRepository;
            _logger = logger;
            _logger.LogInformation("Generator Created");
        }


        /// <summary>
        /// Starts generating messages in thread with 1000ms period
        /// </summary>
        public void Start()
        {
            _logger.LogInformation("Generator Started");
            _stop = false;
            _thread = new Thread(() => { GenerateNewMessage(5000); });
            _thread.Start();
        }

        /// <summary>
        /// stop thread
        /// </summary>
        public void Stop()
        {
            _stop = true;
            _thread.Join();
            _thread = null;
        }


        /// <summary>
        /// Returns random string of given length
        /// </summary>
        /// <param name="length"> given length </param>
        /// <returns> random string </returns>
        private string RandomString(int length)
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        /// <summary>
        /// Creates new Message with random text
        /// </summary>
        /// <param name="period"> period between messages </param>
        public async void  GenerateNewMessage(int period)
        {
            while (!_stop)
            {
                await MessageRepository.PushOutgoingMessageAsync(new Message
                {
                    Status = MessageStatus.Created,
                    Text = RandomString(20),
                });
                Thread.Sleep(period);
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Stop();
            return Task.CompletedTask;
        }
    }
}