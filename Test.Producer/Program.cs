﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Shared;
using Test.Database.Extensions;
using Test.Database.Repositories;

namespace Test.Producer
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddEnvironmentVariables();

                    if (args != null)
                    {
                        config.AddCommandLine(args);
                    }
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddOptions();
                    services.Configure<Config>(hostContext.Configuration.GetSection("Producer"));

                    services.AddTestDatabase(opt =>
                        opt.ConnectionString = "Host=localhost;Database=TestDb;Username=postgres;Password=postgres");

                    services.AddTransient<IMessageRepository, SqlMessageRepository>();
                    services.AddSingleton<IHostedService, Generator>();
                    services.AddSingleton<IHostedService, Publisher>();
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                });


            await builder.RunConsoleAsync();
        }
    }
}