﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NATS.Client;
using Newtonsoft.Json;
using Shared;
using Shared.Models;
using Test.Database.Repositories;

namespace Test.Producer
{
    public class Publisher : BasePublisher, IHostedService
    {
        private readonly ILogger<Publisher> _logger;
        private readonly IOptions<Config> _config;
        private readonly IServiceProvider _provider;

        private IMessageRepository MessageRepository { get; }
        public string Url { get; }
        public string TargetSubject { get; set; }

        public CancellationTokenSource CancelTokenSource { get; set; }


        /// <summary>
        /// Constructor. Initializes publisher object.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="config"></param>
        /// <param name="messageRepository"></param>
        public Publisher(ILogger<Publisher> logger,
            IOptions<Config> config,
            IMessageRepository messageRepository)
        {
            _logger = logger;
            _config = config;
            MessageRepository = messageRepository;

            TargetSubject = config?.Value.TargetSubject ?? "Consumer";
            Url = config?.Value.Url ?? "localhost:4222";
        }


        /// <summary>
        /// Continuously tries to send all queued messages if Connection is available
        /// </summary>
        private async Task SendQueuedMessagesAsync(CancellationToken ctx)
        {
            while (!ctx.IsCancellationRequested)
            {
                try
                {
                    if (Connection != null && (Connection.State != ConnState.CLOSED))
                    {
                        var msg = await MessageRepository.PeekIntoFirstOutgoingMessageAsync();
                        if (msg != null)
                        {
                            msg.SendTime = DateTime.UtcNow;
                            SendMessage(msg);
                            await MessageRepository.PopOutgoingMessageAsync();
                        }
                    }
                    else
                    {
                        EstablishConnection(Url);
                        await Task.Delay(1000, ctx);
                    }
                }
                catch (Exception e)
                {
                    _logger?.LogError($"{e.Message} {e.StackTrace}");
                    await Task.Delay(1000, ctx);
                }
            }
        }

        /// <summary>
        /// Tries to establish connection to specified server
        /// </summary>
        /// <param name="url"> server's url. Default is localhost:4222 </param>
        public void EstablishConnection(string url)
        {
            _logger?.LogInformation($"Trying to connect to {url}");
            try
            {
                InitializeConnection(url);
            }
            catch (Exception e)
            {
                _logger?.LogError($"Exception occured while trying to connect {e.Message}");
            }
        }

        /// <summary>
        /// Tries to send specified message and receive response. If fails throws exception
        /// </summary>
        /// <param name="message"> message to send </param>
        public void SendMessage(Message message)
        {
            message.SendTime = DateTime.UtcNow;
            var req = new SendMessageRequest(message);
            var json = JsonConvert.SerializeObject(req);


            var responseBytes = Exchange(json, TargetSubject, 5000);
            var br = JsonConvert.DeserializeObject<BaseResponse>(responseBytes);

            if (br.Status != "Successful")
            {
                throw new Exception(br.Status);
            }

            _logger?.LogDebug(
                $"Message sent: Id = {req.Message.Id} Text = {req.Message.Text} SendTime {req.Message.SendTime} DbHash {req.Message.DbHash}");
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger?.LogInformation("Starting service: " + SubjectName);

            CancelTokenSource = new CancellationTokenSource();
            var token = CancelTokenSource.Token;

            Task.Run(() => SendQueuedMessagesAsync(token), token);

            return Task.CompletedTask;
        }


        public Task StopAsync(CancellationToken cancellationToken)
        {
            CancelTokenSource.Cancel();
            _logger?.LogInformation("Stopping service.");
            return Task.CompletedTask;
        }
    }
}