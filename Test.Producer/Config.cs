﻿namespace Test.Producer
{
    public class Config
    {
        public string TargetSubject { get; set; }

        public string Url { get; set; }
    }
}