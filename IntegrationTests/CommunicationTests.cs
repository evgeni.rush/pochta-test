using System;
using System.Threading;
using NUnit.Framework;
using Shared;
using Shared.Models;
using Test.Consumer.API.Services;
using Test.Producer;

namespace IntegrationTests
{
    public class Tests
    {
        private Publisher Publisher { get; set; }
        private Handler Handler { get; set; }

        [SetUp]
        public void Setup()
        {
            Publisher = new Publisher(null, null, null);
            Handler = new Handler(null, null, null);
        }

        [TearDown]
        public void TearDown()
        {
            Publisher.Dispose();
            Handler.Dispose();
        }

        [Test]
        [NonParallelizable]
        public void SendsMessageAndReceivesSuccessfulResponse()
        {
            Publisher.EstablishConnection("localhost:4222");
            Handler.StartAsync(CancellationToken.None);
            Publisher.SendMessage(new Message());
            Handler.StopAsync(CancellationToken.None);

            Assert.Pass();
        }

        [Test]
        [NonParallelizable]
        public void SendsMessageAndReceivesTimeout()
        {
            Handler.StopAsync(CancellationToken.None);
            Publisher.TargetSubject = "Not listened";
            Publisher.EstablishConnection("localhost:4222");

            try
            {
                Publisher.SendMessage(new Message());
            }
            catch (Exception e)
            {
                if (e.Message.Contains("Response not received in specified time"))
                    Assert.Pass();
            }

            Assert.Fail();
        }

    }
}

